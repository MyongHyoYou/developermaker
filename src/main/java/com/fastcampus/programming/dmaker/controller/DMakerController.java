package com.fastcampus.programming.dmaker.controller;

import com.fastcampus.programming.dmaker.dto.CreateDeveloper;
import com.fastcampus.programming.dmaker.dto.DeveloperDetailDto;
import com.fastcampus.programming.dmaker.dto.DeveloperDto;
import com.fastcampus.programming.dmaker.dto.EditDeveloper;
import com.fastcampus.programming.dmaker.service.DMakerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Harry M. You
 */

// 해당 class를 RestController라는 Bean으로 등록!
// 가장 기본이 되는 Controller는 그냥 @Controller이지만 REST타입의 요청을 받을 수 있도록 @RestController를 준다.
// (DmakerApplication이라는 레고 판에 Bean들을 하나씩 조립시키는 것인데 RestController 타입의 Bean을 조립시키는 것)
@Slf4j
@RestController
@RequiredArgsConstructor
public class DMakerController {
    private final DMakerService dMakerService;

    @GetMapping("/developers")
    public List<DeveloperDto> getAllDevelopers() {
        // GET /developers HTTP/1.1

        log.info("GET /developers HTTP/1.1");

        return dMakerService.getAllDevelopers();
    }

    @GetMapping("/developer/{memberId}")
    public DeveloperDetailDto getDeveloperDetail(
            @PathVariable String memberId
    ) {
        log.info("memberId : " + memberId);

        return dMakerService.getDeveloperDetail(memberId);
    }

    //@GetMapping("/create-developer")
    @PostMapping("/create-developer")
    public CreateDeveloper.Response createDeveloper(
            @Valid @RequestBody CreateDeveloper.Request request
    ) {
        // GET /create-developer HTTP/1.1
        log.info("request : {" + request + "}");

        return dMakerService.createDeveloper(request);
    }

    @PutMapping("/developer/{memberId}")
    public DeveloperDetailDto editDeveloper (
            @PathVariable String memberId,
            // 수정 요청 시 수정될 값을 받아야하기 때문에 넣어줌
            @Valid @RequestBody EditDeveloper.Request request
    ){
        log.info("PUT /create-developer HTTP/1.1");

        return dMakerService.editDeveloper(memberId, request);
    }
}
