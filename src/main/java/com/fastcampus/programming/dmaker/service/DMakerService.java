package com.fastcampus.programming.dmaker.service;

import com.fastcampus.programming.dmaker.dto.CreateDeveloper;
import com.fastcampus.programming.dmaker.dto.DeveloperDetailDto;
import com.fastcampus.programming.dmaker.dto.DeveloperDto;
import com.fastcampus.programming.dmaker.dto.EditDeveloper;
import com.fastcampus.programming.dmaker.entity.Developer;
import com.fastcampus.programming.dmaker.exception.DMakerErrorCode;
import com.fastcampus.programming.dmaker.exception.DMakerException;
import com.fastcampus.programming.dmaker.repository.DeveloperRepository;
import com.fastcampus.programming.dmaker.type.DeveloperLevel;
import com.fastcampus.programming.dmaker.type.DeveloperSkillType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.fastcampus.programming.dmaker.exception.DMakerErrorCode.*;

/**
 * @author Harry M. You
 */
@Service
@RequiredArgsConstructor
@Transactional
public class DMakerService {
    private final DeveloperRepository developerRepository;

    @Transactional
    public CreateDeveloper.Response createDeveloper(CreateDeveloper.Request request) {
        // private 메소드를 생성하여 비즈니스 validation을 진행한다.
        validateCreateDeveloperRequest(request);

        // Builder를 통해 각각의 데이터들을 삽입해준다.
        Developer developer = Developer.builder()
                .developerLevel(request.getDeveloperLevel())
                .developerSkillType(request.getDeveloperSkillType())
                .experienceYears(request.getExperienceYears())
                .memberId(request.getMemberId())
                .name(request.getName())
                .age(request.getAge())
                .build();

        // 작성된 developer 객체를 developerRepository를 통해 DB에 저장한다.
        developerRepository.save(developer);

        return CreateDeveloper.Response.fromEntity(developer);
    }

    private void validateCreateDeveloperRequest(CreateDeveloper.Request request) {
        // business validatioin
        validateDeveloperLevel(
                request.getDeveloperLevel(),
                request.getExperienceYears()
        );

        Optional<Developer> developer = developerRepository.findByMemberId(request.getMemberId());

        if(developer.isPresent())
            throw new DMakerException(DUPLICATED_MEMBER_ID);
    }

    public List<DeveloperDto> getAllDevelopers() {
        return developerRepository.findAll()
                // fromEntity로부터 나온 결과값을 map에 해준다.
                .stream().map(DeveloperDto::fromEntity)
                // stream된 값을 List로 collect한다. (List<DeveloperDto>를 리턴해야하기 때문에)
                .collect(Collectors.toList());
    }

    public DeveloperDetailDto getDeveloperDetail(String memberId) {
        // developer entity를 가져온다.
        return developerRepository.findByMemberId(memberId)
                .map(DeveloperDetailDto::fromEntity)
                // findByMemberId를 통해 가져온 developer entity가 존재하지 않으면 exception을 발생시킨다.
                .orElseThrow(() -> new DMakerException(NO_DEVELOPER));
    }

    // developer entity에 값을 담아주는 걸로 끝나면 entity에 있는 값만 수정이 되고 DB에는 반영이 되지 않는다.
    // Transactional 키워드를 통해서 entity에 있는 값들을 담아준 다음에 데이터 체크를 하고 그 다음에 DB에 커밋을 한다.
    @Transactional
    public DeveloperDetailDto editDeveloper(String memberId, EditDeveloper.Request request) {

        validateDeveloperLevel(request.getDeveloperLevel(), request.getExperienceYears());

        Developer developer = developerRepository.findByMemberId(memberId).orElseThrow(
                () -> new DMakerException(NO_DEVELOPER)
        );

        developer.setDeveloperLevel(request.getDeveloperLevel());
        developer.setDeveloperSkillType(request.getDeveloperSkillType());
        developer.setExperienceYears(request.getExperienceYears());

        return DeveloperDetailDto.fromEntity(developer);
    }

    private void validateDeveloperLevel(DeveloperLevel developerLevel, Integer experienceYears) {
        if (developerLevel == DeveloperLevel.SENIOR
                && experienceYears < 10) {
            // 회사에서 만드는 특정 비즈니스 앱에서는 Java에 내장된 RuntimeException을 쓰는 것보다는
            // 직접 Exception을 정의해주는 것이 좋다.
            //throw new RuntimeException("SENIOR level needs 10 years of experience!");
            throw new DMakerException(LEVEL_EXPERIEND_YEARS_NOT_MATCHED);
        }

        if(developerLevel == DeveloperLevel.JUNGNIOR
        && ( experienceYears < 4 || experienceYears > 10 )) {
            throw new DMakerException(LEVEL_EXPERIEND_YEARS_NOT_MATCHED);
        }

        if (developerLevel == DeveloperLevel.JUNIOR && experienceYears > 4) {
            throw new DMakerException(LEVEL_EXPERIEND_YEARS_NOT_MATCHED);
        }
    }
}