package com.fastcampus.programming.dmaker.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Harry M. You
 */
@Getter
@AllArgsConstructor
public enum DMakerErrorCode {
    // 아래와 같이 각 에러코드를 정의해주고 각 에러 코드에 해당하는 메시지를 같이 매칭해준다.
    NO_DEVELOPER("해당되는 개발자가 없습니다."),
    DUPLICATED_MEMBER_ID("Member Id가 중복되는 개발자가 있습니다."),
    LEVEL_EXPERIEND_YEARS_NOT_MATCHED("개발자 레벨과 연차가 맞지 않습니다"),

    INTERNAL_SEREVER_ERROR("서버에 오류가 발생했습니다."),
    INVALID_REQUEST("잘못된 요청입니다.")
    ;

    private final String message;
}
